#**Denis Dias de Lima**

[denis@concatenum.com](mailto:denis@concatenum.com)

[br.linkedin.com/in/denisdl](http://br.linkedin.com/in/denisdl)

####Full Stack Developer / Analista de Sistemas / Gerente de Projetos / Gerente de TI

Mais de 25 anos de atuação profissional na área de TI, com experiência em análise de requisitos e desenvolvimento web & software, gestão de projetos, negociação de contratos, apresentações técnicas, análise estatística, treinamento e suporte ao usuário (remoto e on-site), e com ótima habilidade de coordenação de equipes técnicas ou multidisciplinares.

**Experiente** em desenvolvimento web com PHP, PostgreSQL/MySQL, HTML/CSS nos padrões W3C, XML/JSON, frameworks Bootstrap, Foundation e Pure, JQuery, Ubuntu Linux e FreeBSD, scripts Zsh/Bash, Apache/NGINX e certificados SSL, Markdown, expressões regulares (regex) e tratamento de dados (programação de filtros, conversores, extratores, formatadores), MS-Excel e similares, MS-Project.

**Ótimos conhecimentos** em controle de versão de código-fonte com Git (SourceTree, GitKraken, Github Desktop, gitg) em ambientes Windows, Linux e OS X, cloud computing com Amazon Web Services (AWS), metodologia ágil Scrum, Python, UML, modelos relacionais, CMS (Wordpress, Joomla, Drupal, Wikimedia), e-commerce (Woocomerce, OpenCart, Magento), NoSQL/MongoDB, intermediação de pagamentos (PagSeguro e equivalentes), coordenação de equipes e processos.

**Compreensão dos fundamentos** de Redis, AWS DynamoDB, AWS ElasticCache, Microsoft Azure, ITIL v3, Lean IT, DevOps.

####**Idiomas**
* **Inglês** avançado na leitura e na conversação, com experiência comprovada na liderança de negociações internacionais.
* Excelente domínio do **Português** nativo, com facilidade para redações e relatórios técnicos.  
Ótima oratória para treinamentos, apresentações e palestras, com facilidade para transmitir conhecimentos nos dois idiomas.

####**Histórico Profissional**

**Full Stack Developer** - desde Setembro/2014 para a empresária Lucilia Diniz  

* Análise, documentação, manutenção, monitoramento e otimização do back-end de TI do website [luciliadiniz.com](http://luciliadiniz.com) em instâncias de cloud computing na Amazon Web Services (AWS), com EC2/Ubuntu Server, RDS/MySQL, CloudFront, S3, Route 53, Glacier, SNS, CloudWatch, Memcached, Nginx e Varnish Cache, apto para receber até 9 mil leitores simultâneos e mais de 11 milhões de pageviews por mês.
* Desenvolvimento de front-end responsivo desktop e mobile em PHP, HTML5, CSS3/LESS, Bootstrap, Pure, Javascript, JQuery, Highcharts, Chart.js, Wordpress.
* Codificação de Search Engine Optimization (SEO) de acordo com planejamento estratégico da agência de marketing digital.
* Adequação técnica da programação do site às recomendações do relatório de aconselhamento dos consultores jurídicos.
* Desenvolvimento de web crawler e banco de dados para projeção de crescimento dos perfis oficiais no Facebook, YouTube e Instagram, utilizando as APIs das três redes sociais.
* Desenvolvimento de software e banco de dados para análise estatística de engajamento dos seguidores no Instagram e no Facebook, usando as APIs de cada rede social e as APIs do Google Shortener e Bitly.
* Gerenciamento de projetos de TI feitos com desenvolvedores terceirizados para demandas específicas, como apps para iOS e Android, social webgame, interface de chat.
* Curadoria de conteúdo e programação da newsletter por email, enviada semanalmente para mais de 115 mil assinantes.
* Apoio na pesquisa de novos artigos e conteúdos para o site e redes sociais.
* Planejamento e coordenação de todo o processo de produção dos conteúdos em vídeo para Instagram e YouTube.

**Gerente de Projetos** - Outubro/2011 até Setembro/2014 na Apligraf Aplicativos & Gráficos Ltda  
(software house especializada para investidores da bolsa de valores e mercado financeiro)  

_Atividades técnicas_  

* Planejamento, desenvolvimento e implementação do hotsite [asw.apligraf.com.br](http://asw.apligraf.com.br).  
* Manutenção e atualização do site [apligraf.com.br](http://apligraf.com.br).  
* Implantação de um wiki de uso interno para documentação de procedimentos administrativos, suporte técnico, análise da concorrência, precificação de produtos e serviços, e relatórios gerenciais.  
* Treinamento de uso de software de análise técnica do mercado de capitais para mais de 100 funcionários da equipe de atendimento aos clientes do Banco Itaú, além de outros treinamentos de menor escala para instituições diversas.  
* Monitoramento da presença online da empresa nas redes sociais Twitter, Facebook e Linkedin, com produção de conteúdo e atendimento de seguidores/fãs.  
* Suporte técnico de nível II para todos os produtos e serviços da empresa.  
* Especialista em Market Data (PUMA, Broadcast, Bloomberg Professional, ESignal, Reuters, Enfoque, CMA), softwares de análise técnica (Metastock, MetaTrader e dezenas de outros), de análise fundamentalista (Economática, SHB3) e conteúdo para homebrokers e sites de relações com investidores (RI).

_Atividades de gestão_  

* Participação ativa junto aos sócios-diretores na criação de produtos, nas definições de estratégias comerciais, na análise SWOT do negócio e também na reorganização de processos administrativos da empresa.  
* Prospecção e negociação de contratos e SLAs com mais de 200 companhias nacionais de médio e grande porte.  
* Contato inicial, negociação e assinatura de contrato de parceria com Barchart.com Inc. de Chicago/EUA.  
* Principal contato de relacionamento com o mais importante parceiro de negócios (MZ Group, multinacional presente em seis países), responsável desde a definição de workflows entre as equipes operacionais até a definição de condições contratuais com as diretorias.  
* Elaboração das minutas de contratos e aditivos, em parceria com o responsável jurídico.
* Relatórios periódicos de atividades realizadas para os sócios-diretores.  

**Analista de Sistemas** - Janeiro/2000 até Setembro/2011 na Apligraf Aplicativos & Gráficos Ltda.  

* Implementação de serviços web de informações financeiras e de ativos das bolsas de valores em mais de 250 sites de bancos, corretoras de valores e companhias abertas.
* Planejamento e implementação de um serviço web por assinatura paga direcionado a investidores individuais do mercado de ações, que em seis meses já representava 11% da carteira total de clientes.
* Planejamento, desenvolvimento e implementação da sexta versão do site [apligraf.com.br](http://apligraf.com.br), publicado em Setembro/2010 e no ar até o momento, com cotações do mercado financeiro atualizadas em tempo real.
* Implantação, administração e monitoramento do parque de servidores FreeBSD, Linux, Windows 2003 e máquinas virtuais, locados em datacenter terceirizado e conectados por LP de dados com a BM&FBovespa.
* Apresentações técnicas sobre as funcionalidades dos softwares de análise técnica do mercado de capitais para clientes e equipes de helpdesk, desde os recursos mais básicos até às soluções de problemas complexos.
* Participação ativa e de liderança na análise de requisitos da intranet da empresa, implementada em 2000 e em uso até hoje (desenvolvida em ASP com PostgreSQL).
* Negociação do contrato de parceria com o MZ Group, que se tornou o principal parceiro de negócios para a empresa.
* Palestrante no seminário "Finanças Online" em 08/12/2000 para uma audiência de mais de 500 pessoas, com o tema "A evolução das ferramentas de análise técnica para o investidor online", e com entrevista de uma página para o jornal especializado “Monitor Mercantil”.
* Seleção, contratação e treinamento de novos analistas de suporte.
* Expositor dos softwares da empresa em cinco edições da ExpoMoney (2006 -2010), a mais importante feira do setor financeiro.

**Analista de Suporte** - Março/1997 até Dezembro/1999 na Apligraf Aplicativos & Gráficos Ltda.  

* Desenvolvimento de um banco de dados em Access e VBA com o cadastro de todos os clientes, contratos e licenças de uso de software; esse trabalho serviu de base para a intranet da empresa, para onde os dados foram migrados em 2000 e seguem em uso até hoje.
* Desenvolvimento da versão WML/WAP do website da empresa, com cotações de ativos do mercado financeiro atualizados automaticamente a cada 15 minutos, um marco pioneiro na internet brasileira.
* Testes dos softwares desenvolvidos pela empresa para prevenção contra o Bug do Milênio.
* Suporte técnico em cursos e eventos promovidos pela empresa (computadores, projetores, etc).
* Suporte técnico para clientes por e-mail, por telefone e também presencial.
* Revisão e diagramação da apostila do curso “Iniciação à Análise Técnica”.

Histórico profissional completo desde 1991 disponível no LinkedIn em [br.linkedin.com/in/denisdl](http://br.linkedin.com/in/denisdl).

####**Educação Técnica e Superior**
* Graduação Superior, Análise e Desenvolvimento de Sistemas (Universidade Nove de Julho - Uninove, conclusão em 2018)
* Pós-Graduação (aluno especial), Advanced School of Internet Technology (IBPI/Coppead UFRJ, 2000)
* Técnico, Qualificação Profissional em Processamento de Dados (Colégio Paralelo, 1995)

####**Principais cursos, palestras e webinars**
* Fundamentos de DevOps (Microsoft Virtual Academy, 2016)
* Fundamentos do ITIL em Gerenciamento de Serviços de TI (TIExames, 2015)
* Fundamentos de Gestão de TI (FGV Online, 2014)
* Lean IT (EUAX, 2014)
* COBIT & Balanced Scorecard com enfoque prático em empresas de outsourcing (Uninove, 2014)
* Espanhol Básico (Uninove, 2013)
* A importância dos testes funcionais e de performance visando a qualidade de software (Uninove, 2012)
* Resolução de conflitos (Uninove, 2012)
* PHP Orientado a Objetos (Tempo Real Eventos, 2009)
* Virtualização com XEN (Tempo Real Eventos, 2008)
